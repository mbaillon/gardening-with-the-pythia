# Gardening with the pythia
This is the repository linked to the paper "gardening with the pythia".
In this repository we provide the formalization of the axiom translation, the branching translation and the weaning translation that lead to the fundamental result : all Baclofen Type Theory (BTT) definable functionals are continuous. 
